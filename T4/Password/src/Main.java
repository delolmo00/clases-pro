import java.util.Random;

public class Main {
	
	static String [] letters= new String [] {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
	static String [] numbers= new String [] {"0","1","2","3","4","5","6","7","8","9"};
	static String [] signs= new String [] {"<","$","&","/","(","=","!","�","<"};
	static Random random = new Random();
	public static void main(String[] args) {
		
		generar (5,3,1);

	}
	
	static String generarLetters (int num) {
		String resultado = "";
		for (int i=0 ; i< num; i++) {
			int pos = random.nextInt(10);
			resultado = resultado +letters[pos];
			
		}
		System.out.println("Se han generado "+ num + " letras: "+ resultado);
		
		return resultado;
	}
	
	static String generarNumbers (int num) {
		String resultado = "";
		for (int i=0 ; i< num; i++) {
			int pos = random.nextInt(10);
			resultado = resultado +numbers[pos];
			
		}
		System.out.println("Se han generado "+ num + " n�meros : "+ resultado);
		
		return resultado;
	}
	
	static String generarSigns (int num) {
		String resultado = "";
		for (int i=0 ; i< num; i++) {
			int pos = random.nextInt(10);
			resultado = resultado +signs[pos];
			
		}
		System.out.println("Se han generado "+ num + " caracteres especiales: "+ resultado);
		
		return resultado;
	}
	
	static String generar (int letras, int numeros, int signos) {
		String resultado = "";
		String a = generarLetters(letras);
		String b = generarNumbers(numeros);
		String c = generarSigns(signos);
		resultado = a+b+c;
		System.out.println("Su password es "+ resultado);
	
		return resultado;
	}

}
