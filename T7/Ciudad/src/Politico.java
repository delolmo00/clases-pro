
public class Politico extends Ciudadano {

	private String partido;

	public Politico(String nombre, int edad, String peligrosidad,String partido) {
		super(nombre, edad, peligrosidad);
		this.partido = partido;
	}

	public String getPartido() {
		return partido;
	}

	public void setPartido(String partido) {
		this.partido = partido;
	}

	public void proponerLey(String ley) {
		System.out.println(ley);
	}
}
