
public class Antidisturbio extends Ciudadano implements Seguridad{

	private int numPlaca;
	
	

	public Antidisturbio(String nombre, int edad, String peligrosidad, int numPlaca) {
		super(nombre, edad, peligrosidad);
		this.numPlaca = numPlaca;
	}

	public int getNumPlaca() {
		return numPlaca;
	}

	public void setNumPlaca(int numPlaca) {
		this.numPlaca = numPlaca;
	}

	@Override
	public void vigilar() {

System.out.println("Vigilando desde las furgonetas");
		
	}

	@Override
	public void controlarOrden(Ayuntamiento a) {
		int contador = 0;
		for(Ciudadano c : a.getCiudadanos()) {
			if( c.getPeligrosidad()== "Alto") {
				contador++;
				
			}
		}
		if (contador >= 5) {
			System.out.println ("Disparando pelotas de goma");
			apaciguarPeligrosos(a);
		}else {
			System.out.println ("Los policias pueden controlar la situaci�n");
		}
		
	}
	public void apaciguarPeligrosos(Ayuntamiento a) {
		for(Ciudadano c : a.getCiudadanos()) {
			if( c.getPeligrosidad()== "Alto") {
				
				c.setPeligrosidad("Bajo");
			}
		}
		
	}
	
}
