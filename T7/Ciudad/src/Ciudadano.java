
public class Ciudadano {
	
	private String nombre;
	private int edad;
	private String peligrosidad;
	
	
	
	public Ciudadano(String nombre, int edad, String peligrosidad) {
	
		this.nombre = nombre;
		this.edad = edad;
		this.peligrosidad= peligrosidad;
		
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	

	public String getPeligrosidad() {
		return peligrosidad;
	}
	public void setPeligrosidad(String peligrosidad) {
		this.peligrosidad = peligrosidad;
	}
	public void votar(Politico p, Ayuntamiento a) throws MiExcepcion{
		if(this.getEdad()<18) {
			throw new MiExcepcion("Eres menor de edad");
		}else {
			a.getVotos().add(p);
			System.out.println("1 voto para "+ p.getNombre());
		}
		
		
	}
}
