
public class Policia extends Ciudadano implements Seguridad{

	private int num_placa;

	
	public Policia(String nombre, int edad,String peligrosidad,int num_placa) {
		super(nombre,edad, peligrosidad);
		this.num_placa = num_placa;
		
	}
	public int getNum_placa() {
		return num_placa;
	}
	public void setNum_placa(int num_placa) {
		this.num_placa = num_placa;
	}
	
	
	@Override
	public void vigilar() {
		System.out.println("Espero ordenes de los superiores.");
		
	}
	@Override
	public void controlarOrden(Ayuntamiento a) {
		int contador = 0;
		for(Ciudadano c : a.getCiudadanos()) {
			if( c.getPeligrosidad()== "Alto") {
				contador++;
			}
		}
		if (contador >= 3) {
			System.out.println ("Golpeando con las porras");
		}else {
			System.out.println ("No hay peligro. S�lo controlamos la situaci�n");
		}
		
	}
	
	
	
}
