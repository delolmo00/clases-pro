
public class Main {

	public static void main(String[] args) {
		Ayuntamiento a = new Ayuntamiento();
		Policia policia = new Policia("Juan",33,"Bajo",222);
		Antidisturbio antidisturbio = new Antidisturbio("Pepe",40,"Bajo",123);
		
		Politico p1 = new Politico("Ana",44, "Bajo","partido1");
		Politico p2 = new Politico("Pepe",70, "Bajo","partido2");
		Politico p3 = new Politico("Ram�n",60, "Bajo","partido3");
		
		Ciudadano c1 = new Ciudadano("Jana", 20,"Alto");
		Ciudadano c2 = new Ciudadano("Rocio", 20,"Alto");
		Ciudadano c3 = new Ciudadano("Nuria", 20,"Medio");
		Ciudadano c4 = new Ciudadano("Pilar", 20,"Alto");
		Ciudadano c5 = new Ciudadano("Victor", 20,"Alto");
		
		a.censarCiudadano(c1);
		a.censarCiudadano(c2);
		a.censarCiudadano(c3);
		a.censarCiudadano(c4);
		a.censarCiudadano(c5);
		
		try {
			c1.votar(p1, a);
			c2.votar(p1, a);
			c3.votar(p2, a);
			c4.votar(p3, a);
			c5.votar(p1, a);
			
		} catch (MiExcepcion e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("Los votos del partido "+ p1.getPartido()+ " son "+ a.contarVotos(p1));
		System.out.println("Los votos del partido "+ p2.getPartido()+ " son "+ a.contarVotos(p2));
		System.out.println("Los votos del partido "+ p2.getPartido()+ " son "+ a.contarVotos(p3));
		
	}

}
