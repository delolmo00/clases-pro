import java.util.ArrayList;

public class Ayuntamiento {
	
	private ArrayList<Ciudadano> ciudadanos ;
	private ArrayList<Politico> votos;
	

	
	public Ayuntamiento() {
		
		this.ciudadanos = new ArrayList<>();
		this.votos = new ArrayList<>();
	}
	public ArrayList<Ciudadano> getCiudadanos() {
		return ciudadanos;
	}
	public void setCiudadanos(ArrayList<Ciudadano> ciudadanos) {
		this.ciudadanos = ciudadanos;
	}
	public ArrayList<Politico> getVotos() {
		return votos;
	}
	public void setVotos(ArrayList<Politico> votos) {
		this.votos = votos;
	}

	public int contarVotos(Politico p1) {
		int contador = 0;
		for(Politico p: votos) {
			if(p.getNombre()== p1.getNombre()) {
				contador ++;
			}
		}
		
		return contador;
	}
	public void censarCiudadano(Ciudadano c) {
		this.ciudadanos.add(c);
	}
}
