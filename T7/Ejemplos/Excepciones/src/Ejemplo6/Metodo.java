package Ejemplo6;

public class Metodo {

	public void mayorEdad (int edad) throws MiExcepcion{
		if (edad >= 18) {
			System.out.println("Eres mayor de edad");
		}else {
			throw new MiExcepcion("Eres menor de edad. No puedes continuar");
			
		}
	}
	
}
