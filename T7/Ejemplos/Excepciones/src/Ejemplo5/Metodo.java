package Ejemplo5;

public class Metodo {
	
	public void excepcionNull() throws NullPointerException{
		
		String palabra =  null;
		System.out.println(palabra.length());
	}
	
	public void excepcionArray() throws ArrayIndexOutOfBoundsException{
		String[] palabras = new String[] { "Uno", "dos", "Tres" };
		
		for (int i = 0; i<= 3;i++) {
			System.out.println(palabras[i]);
		}
	}

}
