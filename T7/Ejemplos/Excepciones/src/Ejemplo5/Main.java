package Ejemplo5;

public class Main {

	public static void main(String[] args) {
	
		Metodo m = new Metodo();
		
		try {
		m.excepcionNull();

	}catch(NullPointerException e) {
		System.out.println ("Objeto nulo");
	}
		
		
		try {
			
			m.excepcionArray();
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Se sale del array");
		}

}
}
