package Ejemplo3;

public class Main {

	public static void main(String[] args) {

		try {
			int division = 4 / 0;
			System.out.println(division);

		} catch (ArithmeticException e) {
			System.out.println("No puede dividir por cero");
		} finally {
			System.out.println("Aqui mi app sigue corriendo");
		}
	}
}
