package misexcepciones;

public class CuentaMalException extends Exception {

    public CuentaMalException(String mensaje) {
        super(mensaje);
    }

}
