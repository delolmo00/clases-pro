package misexcepciones;

public class MensajeMalException extends Exception {

    public MensajeMalException(String mensaje) {
        super(mensaje);
    }

}
