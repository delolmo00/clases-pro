package clases;
/*
Programa una aplicación Java que permita gestionar un sistema de correo
electrónico ficticio. Para ello, debes tener en cuenta que:
• Una cuenta de correo electrónico está formada por:
▪ Una dirección de correo única
▪ Un nombre del propietario
▪ Una bandeja de entrada (colección de mensajes)
• Existe una colección de direcciones de correo pública en la que se van
introduciendo todas y cada una de las cuentas nuevas creadas con éxito.
• Una cuenta no puede crearse bajo ningún concepto si pasa algo de esto:
▪ La dirección de correo ya está cogida.
▪ El nombre del propietario está vacío o vale null.
• Una cuenta puede enviar un mensaje a otra
• Un mensaje consta de:
▪ Dirección de destino
▪ Asunto
▪ Cuerpo
• Un mensaje no puede crearse únicamente si su dirección de destino vale null,
está vacía o no tiene la forma:
algo@algo.algo
• Cuando se envía un mensaje, se comprueba que su dirección de destinatario es
válida (está en la colección pública anteriormente mencionada). Si da negativo,
dará un error en el envío. Si da positivo, entonces se añadirá a la bandeja de
entrada de la cuenta destinataria.
• Las cuentas tienen la opción de ver un listado con los asuntos y direcciones
origen de los mensajes de su bandeja de entrada.
• Si se quiere ver el contenido de un mensaje concreto, también es posible
indicando su índice en la colección.
Notas
- Usar excepciones para el tratamiento de errores de construcción o ejecución de
métodos.
- Usar encapsulación.
- Usar las colecciones adecuadas
*/

import java.util.ArrayList;
import misexcepciones.CuentaMalException;
import misexcepciones.MensajeMalException;

public class CuentaCorreo {
    public static final ArrayList<String> CUENTAS = new ArrayList<>();

    private final String direccion;
    private final String nombre;
    private final ArrayList<Mensaje> bandeja;

    public CuentaCorreo(String d, String n) throws CuentaMalException {

        if (d == null) {
            throw new CuentaMalException("La dirección indicada vale 'null'.");
        } else if (d.isEmpty()) {
            throw new CuentaMalException("La dirección indicada está vacía.");
        } else if (n == null) {
            throw new CuentaMalException("El nombre indicado vale 'null'.");
        } else if (n.isEmpty()) {
            throw new CuentaMalException("El nombre indicado está vacío.");
        } if (d.split("@").length != 2 || d.split("\\.").length != 2) {
            throw new CuentaMalException("La dirección no cumple el formato «algo@algo.algo».");
        } else if (CUENTAS.contains(d)) {
            throw new CuentaMalException("La dirección ya está cogida.");
        }

        CUENTAS.add(d);

        direccion = d;
        nombre = n;
        bandeja = new ArrayList<>();
    }

    public String getDireccion() {
        return direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void recibir(Mensaje m) {

        if (m == null) {
            System.out.println("El mensaje recibido vale 'null'.");
        } else {
            bandeja.add(m);
        }

    }

    public void enviar(CuentaCorreo dest, String asunto, String cuerpo) throws MensajeMalException, CuentaMalException {
        dest.recibir(new Mensaje(direccion, dest.getDireccion(), asunto, cuerpo));
    }

    public void verBandeja() {

        for (int i = 0; i < bandeja.size(); i++) {
            System.out.println(i + "\t" + bandeja.get(i).getOrigen() + "\t" + bandeja.get(i).getAsunto());
        }

    }

    public void verMensaje(int idx) {

        if (idx >= 0 && idx < bandeja.size()) {
            System.out.println("Mensaje de: " + bandeja.get(idx).getOrigen());
            System.out.println("Asunto:     " + bandeja.get(idx).getAsunto());
            System.out.println("Contenido:  " + bandeja.get(idx).getCuerpo());
        } else {
            System.out.println("No existe el mensaje.");
        }

    }

}
