package clases;

import java.time.LocalDate;
import java.time.Month;

public class Persona implements Comparable {
	public static int contador = 0;
	private int codigo_cliente;
	private String nombre;
	private LocalDate nacimiento;
	private int nivel_urgencia;

	public Persona(int year, int month, int day, String nombre, int urgencia) throws misexcepciones {
		//contador = contador + 1;
		this.nombre = nombre;
		if (urgencia > 5) {
			throw new misexcepciones("Error no puedes registrar una urgencia mayor de 6");
		} else {
			this.nivel_urgencia = urgencia;
		}

		this.nacimiento = LocalDate.of(year, month, day);
		contador++;
		this.codigo_cliente = contador;

	}

	public int getCodigo_cliente() {
		return codigo_cliente;
	}

	public void setCodigo_cliente(int codigo_cliente) {
		this.codigo_cliente = codigo_cliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNivel_urgencia() {
		return nivel_urgencia;
	}

	public void setNivel_urgencia(int nivel_urgencia) {
		this.nivel_urgencia = nivel_urgencia;
	}

	@Override
	public int compareTo(Object miObjeto) {

		Persona otraPersona = (Persona) miObjeto;

		if (this.getNivel_urgencia() < otraPersona.getNivel_urgencia()) {
			return -1;
		} else if (this.getNivel_urgencia() > otraPersona.getNivel_urgencia()) {
			return 1;
		} else {
			return 0;
		}
	}

}
