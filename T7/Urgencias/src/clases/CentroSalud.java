package clases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class CentroSalud {

	private ArrayList<Persona> listaPrioridad = new ArrayList<>();
	private ArrayList<Persona> box = new ArrayList<>();

	public void recibir(Persona p1) {
		listaPrioridad.add(p1);
		// El paciente «C», llamado «X», se pone a la cola en el puesto «P».

		System.out.println("El paciente " + p1.getCodigo_cliente() + " llamado " + p1.getNombre()
				+ " se pone a la cola en el puesto " + listaPrioridad.indexOf(p1));
		Collections.sort(listaPrioridad);
	}

	public void imprimir() {
		for (Persona aux : listaPrioridad) {
			System.out.println(aux.getNombre());
		}
	}

	public void pasaralBox() throws misexcepciones {
		int posicion = listaPrioridad.size() - 1; // saco el indice m�ximo

		Persona p1 = listaPrioridad.get(posicion);

		if (listaPrioridad.contains(p1)) {
			box.add(p1);
			listaPrioridad.remove(p1);
			System.out.println("El paciente " + p1.getNombre() + " entra en el box.");

		} else {
			throw new misexcepciones("El paciente no est� en la lista de espera");
		}

	}

	public void salirDelBox() throws misexcepciones {

		Persona p1 = box.get(0);

		if (box.contains(p1)) {
			box.remove(p1);
			System.out
					.println("El paciente " + p1.getCodigo_cliente() + " llamado " + p1.getNombre() + " fue atendido.");

		} else {
			throw new misexcepciones("El paciente no est� en el box");

		}

	}

	public void salirDelBoxSegunPaciente(Persona p1) throws misexcepciones {

		if (box.contains(p1)) {
			box.remove(p1);
			System.out
					.println("El paciente " + p1.getCodigo_cliente() + " llamado " + p1.getNombre() + " fue atendido.");

		} else {
			throw new misexcepciones("El paciente no está en el box");

		}

	}

}
