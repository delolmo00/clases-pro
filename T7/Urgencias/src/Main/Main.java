
package Main;

import clases.CentroSalud;
import clases.Persona;
import clases.misexcepciones;

public class Main {
	public static void main(String[] args) {

		CentroSalud centro = new CentroSalud();

		try {
			Persona p1 = new Persona(2010, 2, 3, "Juan", 5);
			Persona p2 = new Persona(1998, 5, 12, "Pepe", 1);
			Persona p3 = new Persona(1990, 6, 16, "Luis", 2);
			Persona p4 = new Persona(1998, 3, 12, "Juancho", 5);

			centro.recibir(p1);

			centro.recibir(p2);
			centro.recibir(p3);
			centro.recibir(p4);
			centro.imprimir();
			centro.pasaralBox();
			centro.pasaralBox();
			centro.pasaralBox();
			centro.pasaralBox();
			centro.salirDelBox();
			centro.salirDelBox();
			centro.salirDelBox();

		} catch (misexcepciones e1) {

			System.err.println("Error");
		} 

		

	}
}
