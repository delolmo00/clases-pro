
public class Juez extends Persona{
	private int numCombates;

	public Juez(String nombre, int edad, double peso, int numCombates) {
		super(nombre, edad, peso);
		this.numCombates = numCombates;
	}

	public int getNumCombates() {
		return numCombates;
	}

	public void setNumCombates(int numCombates) {
		this.numCombates = numCombates;
	}

	public void arbitrar(Boxeador b1, Boxeador b2) {
		b1.pelear(b1, b2);
		
		if(b1.getVida()>b2.getVida()) {
			System.out.println("Ha ganado el boxeador "+ b1.getNombre());
			b1.getRivalesGanados().add(b2);
		}else {
			System.out.println("Ha ganado el boxeador "+ b2.getNombre());
			b2.getRivalesGanados().add(b1);
		}
		numCombates++;
	}

	
}
