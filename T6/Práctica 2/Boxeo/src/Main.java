import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       
    	Scanner sc = new Scanner (System.in);
    	System.out.println("Indique 1 para crear boxeadores y pelear. 2 para salir.");
    	try {
    		int respuesta = sc.nextInt();
    		if(respuesta == 1) {
    			Boxeador boxeador1 = new Boxeador("Floyd Mayweather", 44, 68.00,100);
                Boxeador boxeador2 = new Boxeador("Packiao", 55, 70.00,100);
                Juez juez1 = new Juez("Juez 1",60,70.00,3);
                // Llamamos al m�todo entrenar
                boxeador1.entrenar();

                boxeador1.pelear(boxeador1,boxeador2);
                juez1.arbitrar(boxeador1, boxeador2);
                boxeador1.mostrarRivalesGanados();
                boxeador2.mostrarRivalesGanados();
    		}else {
    			System.out.println("Hasta luego");
    		}
    	}catch(InputMismatchException e) {
    		System.out.println("Debes de introducir un n�mero -- 1 � 2");
    		
    	}catch(Exception e) {
    		System.out.println(e.getMessage());
    		
    	}
    	
    	
        
       
    }
}

