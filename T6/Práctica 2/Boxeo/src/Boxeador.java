import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Boxeador extends Persona implements Lucha{
   
    private int vida;
    private ArrayList <Boxeador>rivalesGanados = new ArrayList<>();

	public Boxeador(String nombre, int edad, double peso, int vida) throws MiExcepcion{
		super(nombre, edad, peso);
		if (vida >100) {
			throw new MiExcepcion("La vida no puede ser mayor a 100");
		}else {
			this.vida = vida;
		}
		
		
	}
	

	public int getVida() {
		return vida;
	}


	public void setVida(int vida) {
		this.vida = vida;
	}


	public void setRivalesGanados(ArrayList<Boxeador> rivalesGanados) {
		this.rivalesGanados = rivalesGanados;
	}


	public ArrayList getRivalesGanados() {
		return rivalesGanados;
	}

	

	@Override
	public void entrenar() {
		System.out.println("Realizando entrenamiento");
		this.setVida(this.getVida()+10);
		
	}

	public void pelear(Boxeador b1, Boxeador b2) {
		
		
		while((b1.getVida()>0) && (b2.getVida()>0)) {
			darGolpe(b1);
			darGolpe(b2);
			if(b1.rendirse()) {
				break;
			}else if (b2.rendirse()) {
				break;
			}
			
		}
		System.out.println("Despu�s de la pelea: "+ "la vida de"+ b1.getNombre()+" es "+ b1.getVida());
		System.out.println("Despu�s de la pelea: "+ "la vida de"+ b2.getNombre()+" es "+ b2.getVida());
		
	}
	@Override
	public void darGolpe(Boxeador b) {
		Random r = new Random();
		int golpe = r.nextInt(10);
		b.setVida(b.getVida() -golpe);
	}


	@Override
	public boolean rendirse() {
		if(this.getVida()== 2) {
			System.out.println("Me rindo con una vida de "+ this.getVida());
			System.out.println("La vida de "+this.getNombre()+ " "+ this.getVida());
			this.setVida(0);
			return true;
		}else {
			return false;
		}
		
	}
	/*
	public void mostrarRivalesGanados() {
		for(Boxeador b : rivalesGanados) {
			System.out.println(b.getNombre());
		}
	}
	*/
	public void mostrarRivalesGanados(){
		Scanner sc = new Scanner(System.in);
		System.out.println("�Cuantos rivales quieres ver?");
		int numero = sc.nextInt();
		
		try {
		for (int i = 0;i<numero;i++) {
			rivalesGanados.get(i).getNombre();
		}
	}catch(InputMismatchException e) {
		System.out.println("El formato es incorrecto");
	}catch(ArrayIndexOutOfBoundsException e) {
		System.out.println("No hay tantos rivales");
	}
    

  
}
    
}
