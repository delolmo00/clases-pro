import java.util.ArrayList;

public class Administracion {

	private String nombre, direccion;
	private double recaudacion;
	private ArrayList<Boleto> apuestas = new ArrayList<>();
	private  static ArrayList<Boleto> todosBoletos = new ArrayList<> ();
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public double getRecaudacion() {
		return recaudacion;
	}
	public void setRecaudacion(double recaudacion) {
		this.recaudacion = recaudacion;
	}
	public Administracion(String nombre, String direccion, double recaudacion) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
		this.recaudacion = recaudacion;
		
	}
	public ArrayList<Boleto> getApuestas() {
		return apuestas;
	}
	public void setApuestas(ArrayList<Boleto> apuestas) {
		this.apuestas = apuestas;
		
	}
	
	public void expedirApuesta(Boleto apuesta) {
		apuestas.add(apuesta);
		todosBoletos.add(apuesta);
	}
	public double verRecaudacion() {
		double recaudacion=0;
		
		for(Boleto b : apuestas) {
			recaudacion = recaudacion + b.getPrecio();
			
		}
		System.out.println("La recaudacion total es "+ recaudacion);
		return recaudacion;
		
	}
	
	public void compararAciertos(Boleto b1, Boleto b2) {
		
		if (b1 instanceof Primitiva && b2 instanceof Primitiva) {
			Primitiva p1 = (Primitiva)b1;
			Primitiva p2 = (Primitiva)b2;
			int aciertos1 = p1.comprobarAciertos();
			int aciertos2 = p2.comprobarAciertos();
			
			if(aciertos1 > aciertos2) {
				System.out.println("La primitiva 1 tiene m�s aciertos");
			}else {
				System.out.println("La primitiva 2 tiene m�s aciertos");
			}
		}else {
			Euromillones e1 = (Euromillones) b1;
			Euromillones e2 = (Euromillones) b2;
			int aciertos1 = e1.comprobarAciertos();
			int aciertos2 = e2.comprobarAciertos();
			
			if(aciertos1 > aciertos2) {
				System.out.println("La euromillones 1 tiene m�s aciertos");
			}else {
				System.out.println("La euromillones 2 tiene m�s aciertos");
			}
		}
		
		
		
	}
	
}
