import java.util.ArrayList;

public interface Loteria {
	
	public void apostar();

	 public boolean comprobarRepetido(ArrayList<Integer> lista, int numero);
	      
    public void generarResultado();

  
    public void mostrarApuesta() throws MiExcepcion;

    public void mostrarResultado();

    public int comprobarAciertos();
    public void iniciarSorteo();

}
