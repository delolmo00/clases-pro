
public class Main {

	public static void main(String[] args) {
		
		Euromillones e = new Euromillones (123,"Electr�nico");
		Primitiva p = new Primitiva (124,"Papel");
		Administracion a = new Administracion ("Do�a manolita", "Madrid",0);
		
		e.iniciarSorteo();
		p.iniciarSorteo();
		try {
			p.mostrarApuesta();
		} catch (MiExcepcion e1) {
			
			System.out.println(e1.getMessage());
		}
		
		e.apostar();
		p.apostar();
		
		e.generarResultado();
		p.generarResultado();
		
		e.comprobarAciertos();
		p.comprobarAciertos();
	
		/*
		 * 
		 Administracion a1 = new Administracion("Do�a manolita","Calle gran via",0)
		  Administracion a2 = new Administracion("Bruja de Oro","Calle......",0)
		  a1.expedirApuesta(p1)
		  a1.expedirApuesta(e1)
		  a1.verRecaudacion()
		 */
		a.expedirApuesta(p);
		
		
	}

}
