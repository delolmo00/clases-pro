import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Primitiva extends Boleto implements Loteria {
	private static final int MAX_NUMEROS = 6;
	private static final int MAX_VALOR = 49;
	private double precio = 1.50;
	public ArrayList<Integer> apuesta;
    public static ArrayList<Integer> resultado= new ArrayList<>();
   

	public Primitiva(int id, String formato) {
		super(id, formato,1.00);
		this.apuesta = new ArrayList<>();
		 // this.resultado = new ArrayList<>();
	}

	
	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	@Override
	public void apostar() {
		Scanner sc = new Scanner(System.in);
		System.out.println("PRIMITIVA:");
		System.out.println("********");
		for (int i = 0; i < MAX_NUMEROS; i++) {
			System.out.println("Escribe un n�mero");
			int numero = sc.nextInt();
			if (!comprobarRepetido(apuesta, numero)) {
				apuesta.add(numero);
			} else {
				System.out.println("n�mero repetido");
				i--;
			}

		}

	}

	@Override
	public boolean comprobarRepetido(ArrayList<Integer> lista, int numero) {
		return lista.contains(numero);
	}

	@Override
	public void generarResultado() {
		Random random = new Random();
		for (int i = 0; i < MAX_NUMEROS; i++) {
			int numero = random.nextInt(MAX_VALOR);
			if (!comprobarRepetido(resultado, numero)) {
				resultado.add(numero);
			} else {

				i--;
			}
		}

	}

	@Override
	public void mostrarApuesta() throws MiExcepcion {
		if (apuesta.size() == 0) {
			throw new MiExcepcion("No has realizado ninguna apuesta!. El boleto est� vac�o");
		} else {
			System.out.println("Tus n�meros son:");
			System.out.println("******************");
			for (Integer numero : apuesta) {
				System.out.println("N�mero:" + numero);
			}
			System.out.println("******************");

		}

	}

	@Override
	public void mostrarResultado() {
		for (Integer numero : resultado) {
			System.out.println("N�mero:" + numero);
		}

	}

	@Override
	public int comprobarAciertos() {

		int aciertos = 0;
		for (int numero : apuesta) {
			if (resultado.contains(numero)) {
				aciertos++;
			}
		}
		System.out.println("Has acertado :" + aciertos);
		if (aciertos > 4) {
			System.out.println("Enhorabuena. Has ganado bastante plata. Inv�tate a algo, rata!");
		} else {
			System.out.println("Sigues pobre. No pasa nada.");
		}
		return aciertos;

	}
	
	@Override
	public void iniciarSorteo() {
		System.out.println ("Hagan sus apuestas");
		resultado.clear();
	}

}
