import java.util.ArrayList;

public abstract class Boleto {
	private int id;
    private String formato;
    private double precio;
    //public ArrayList<Integer> apuesta;
    //public static ArrayList<Integer> resultado;

    public Boleto(int id, String formato, double precio) {
        this.id = id;
        this.formato = formato;
        this.precio = precio;
        
    }
    

	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	

	
    

}
