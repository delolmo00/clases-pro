
public class Circulo extends Figura{
	
	private double radio, diametro;
	
	public Circulo (double radio) {
		this.radio = radio;
	}

	@Override
	public double calcularArea() {
		this.setArea(Math.PI * Math.pow(this.radio,2));
		System.out.println(this.getArea());
		
		return this.getArea();
	}
	
	public double calcularDiametro() {
		this.diametro = this.radio *2;
		System.out.println(this.diametro);
		return this.diametro;
	}

	
}
