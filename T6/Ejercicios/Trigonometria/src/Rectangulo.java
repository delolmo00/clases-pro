
public class Rectangulo  extends Figura{
	
	private double perimetro;
	private double base, altura;
	
	public Rectangulo (double base, double altura) {
		this.base = base;
		this.altura = altura;
	}
	
	public Rectangulo () {
		
	}

	@Override
	public double calcularArea() {
		
		this.setArea (this.base * this.altura);
		System.out.println(this.getArea());
		return this.getArea();
	}
	
	

}
