
public class Main {

	public static void main(String[] args) {
		
		Circulo circulo = new Circulo (7);
		Rectangulo rectangulo = new Rectangulo (4,7);
		Rectangulo rectangulo2 = new Rectangulo();
		Triangulo triangulo = new Triangulo (4,5);
		
		triangulo.calcularArea();
		rectangulo.calcularArea();
		circulo.calcularArea();
		circulo.calcularDiametro();

	}

}
