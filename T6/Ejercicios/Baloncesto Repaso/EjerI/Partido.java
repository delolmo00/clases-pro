package EjerI;

import java.util.Scanner;

public class Partido {
	private static Scanner tec;
	public static  Equipo local;
	public static Equipo visitante;
	
	public Partido() {
		CrearLocal();
		CrearVisitante();
	}
	
	
	
	
	public static void CrearLocal() {
		System.out.println("--- Equipo Local ---");
		local=new Equipo();

		
	}
	
	
	public static void CrearVisitante() {
		System.out.println("--- Equipo Visitante ---");
	visitante=new Equipo();
		
	}
	
	
	public Partido(String nomLocal, String nomVisitante) {
		local=new Equipo(nomLocal);
	visitante=new Equipo(nomVisitante);
		
	}
	
	public static void Anotar() {
		tec=new Scanner(System.in);
		int num;
		System.out.println("�Qu� equipo anot�?");
		System.out.println("1: " + local.nombre);
		System.out.println("2: " + visitante.nombre);
		num=tec.nextInt();
		switch(num) {
		default: 	System.out.println("No se sumar�n puntos a ning�n equipo"); break;
		case 1: local.anotar(); break;
		case 2: visitante.anotar(); break;
		}
	}
	
	public static void marcador() {
		System.out.println(local.nombre + ": " + local.CalcularPuntos() + " - " + visitante.nombre
				+ ": " + visitante.CalcularPuntos());
	}
	
	public static void verPlantillas() {
		tec=new Scanner(System.in);
		int num;
		System.out.println("De qu� equipo quieres ver la plantilla");
		System.out.println("1: " + local.nombre);
		System.out.println("2: " + visitante.nombre);
		num=tec.nextInt();
		switch(num) {
		default: 	System.out.println("No se mostrar� la plantilla de nadie"); break;
		case 1: System.out.println("Titulares:  ");  local.verTitulares();  System.out.println("Suplentes: "); local.verSuplentes();
		break;
		case 2:   System.out.println("Titulares:  ");  visitante.verTitulares();  System.out.println("Suplentes: "); visitante.verSuplentes(); break;
		
	}
	}
	
	public static void verTitulares() {
		tec=new Scanner(System.in);
		int num;
		System.out.println("De qu� equipo quieres ver los titulares");
		System.out.println("1: " + local.nombre);
		System.out.println("2: " + visitante.nombre);
		num=tec.nextInt();
		switch(num) {
		default: 	System.out.println("No se mostrar�n los titulares de nadie"); break;
		case 1: System.out.println("Titulares:  ");  local.verTitulares(); 
		break;
		case 2:   System.out.println("Titulares:  ");  visitante.verTitulares();  break;
		
	}
		
	}
	
	public static void verEstadisticas() {
		tec=new Scanner(System.in);
		int num;
		System.out.println("De qu� equipo quieres ver las estad�sticas");
		System.out.println("1: " + local.nombre);
		System.out.println("2: " + visitante.nombre);
		num=tec.nextInt();
		switch(num) {
		default: 	System.out.println("No se mostrar�n las estad�sticas"); break;
		case 1: System.out.println("Puntos:  " + local.CalcularPuntos());    System.out.println("Faltas:  " + local.CalcularFaltas());  
		break;
		case 2:   System.out.println("Puntos:  " + visitante.CalcularPuntos());  System.out.println("Faltas:  " + visitante.CalcularFaltas());  
		break;
		
	}
		
	}
	
	public static void sustituir() {
		tec=new Scanner(System.in);
		int num;
		System.out.println("�Qu� equipo quiere realizar una sustituci�n?");
		System.out.println("1: " + local.nombre);
		System.out.println("2: " + visitante.nombre);
		num=tec.nextInt();
		switch(num) {
		case 1: local.sustituir(); break;
		case 2: visitante.sustituir(); break;
		default: System.out.println("No se sustituir� a nadie"); break;
		}	
	}
	
	public static int gestionarOperaciones() {
		int resp;
		tec=new Scanner(System.in);
		System.out.println("--- Operaciones ---");
		System.out.println("1: Sustituci�n");
		System.out.println("2: Canasta");
		System.out.println("3: Falta");
		System.out.println("4: Ver estad�sticas");
		System.out.println("5: Ver equipo titular");
		System.out.println("6: Ver Plantilla");
		System.out.println("7: Ver Marcador");
		System.out.println("8: Resultado del Partido (finalizar partido)");
		System.out.println("Realiza una operaci�n: ");
		resp=tec.nextInt();
		return resp;
	}
	
	public static void faltasPersonales() {
		tec=new Scanner(System.in);
		int num;
		System.out.println("�Qu� equipo cometi� una falta?");
		System.out.println("1: " + local.nombre);
		System.out.println("2: " + visitante.nombre);
		num=tec.nextInt();
		switch(num) {
		case 1: local.faltasPersonales(); break;
		case 2: visitante.faltasPersonales(); break;
		default: System.out.println("No se pondr� una falta"); break;
		}	
		
	}
	
	public static void resultado() {
		marcador(); 
	 System.out.println("El partido ha finalizado");
	 if(local.CalcularPuntos()>visitante.CalcularPuntos()) {
		 System.out.println("Ha ganado el equipo: " + local.nombre); 
	 }
	 else if(local.CalcularPuntos()==visitante.CalcularPuntos()){
		 System.out.println("Empate"); 
		 
	 }
	 else {
		 System.out.println("Ha ganado el equipo: " + visitante.nombre); 
		 
	 }
		
	}

	public static void main(String[] args) {
		CrearLocal();
		CrearVisitante();
		String resp="s";
		do {
			switch(gestionarOperaciones()) {
			default: System.out.println("No se realizar� nada"); break;
			case 1: sustituir();
				break;
			case 2: Anotar();
				break;
			case 3: faltasPersonales();
				break;
			case 4: verEstadisticas();
				break;
			case 5:verTitulares(); 
				break;
			case 6: verPlantillas();
				break;
			case 7: marcador();
				break;
			case 8: resultado(); resp="n";
				break;
			
			}
			
		}while(resp.equalsIgnoreCase("s"));
	}
	


}
