package EjerI;

import java.util.Scanner;



public class Equipo {
	private Scanner tec;
	public Jugadores[] titulares;
	public Jugadores[] suplentes;
	public String nombre;
	
	public Equipo() {
		tec=new Scanner (System.in);
		System.out.println("Cual es el nombre del equipo");
		this.nombre=tec.next();
		this.titulares=new Jugadores[5];
		this.suplentes=new Jugadores[3];
		for(int pos=0;pos<this.titulares.length;pos++) {
			System.out.println("--- Titular " + (pos+1) + " ---");
			this.titulares[pos]=new Jugadores();
		}
		for(int pos=0;pos<this.suplentes.length;pos++) {
			System.out.println("--- Suplente " + (pos+1) + " ---");
			this.suplentes[pos]=new Jugadores();
		}
	}
	
	public Equipo(String n) {
		this.nombre=n;
		this.titulares=new Jugadores[5];
		this.suplentes=new Jugadores[3];
		for(int pos=0;pos<this.titulares.length;pos++) {
			this.titulares[pos]=new Jugadores();
		}
		for(int pos=0;pos<this.suplentes.length;pos++) {
			this.suplentes[pos]=new Jugadores();
		}
	}
	
	public Equipo(String n, Jugadores[]t, Jugadores[]s) {
		this.nombre=n;
		this.titulares=t;
		this.suplentes=s;
	}
	
	public void sustituir() {
		tec=new Scanner(System.in);
		int n;
		int num;
		Jugadores asist=new Jugadores("vacio");
		verTitulares();
		System.out.println("A que titular quieres sustituir?");
		n=tec.nextInt();
		if(n<=0 || n>this.titulares.length   ) {
			System.out.println("No se har� la sustituci�n");
		}
		else {
			n=n-1;
			verSuplentes();
			System.out.println("Por qu� suplente quieres sustituirlo?");
			num=tec.nextInt();
			if(num<=0 || num>this.suplentes.length) {
				System.out.println("No se har� la sustituci�n");
			}
			else {
				num=num-1;
				if(this.suplentes[num].faltas>=5) {
					System.out.println("No se puede cambiar por ese jugador, ya que tiene 5 faltas");
					if(this.titulares[n].faltas>=5) {
						Sustituir(n);
					}
					
				}
				else {
					asist=this.titulares[n];
					this.titulares[n]=this.suplentes[num];
					this.suplentes[num]=asist;
				}
				
			}	
		}	
	}
	
	public void Sustituir(int posT) {
		Jugadores asist=new Jugadores("vacio");
		tec=new Scanner(System.in);
		int num;
		verSuplentes();
		System.out.println("Por qu� suplente quieres sustituirlo?");
		num=tec.nextInt();
		if(num<=0 || num>this.suplentes.length) {
			System.out.println("No se har� la sustituci�n");
		}
		else {
			num=num-1;
			if(this.suplentes[num].faltas>=5) {
				System.out.println("No se puede cambiar por ese jugador, ya que tiene 5 faltas");
				if(this.titulares[posT].faltas>=5) {
					Sustituir(posT);
				}
				
			}
			else {
				asist=this.titulares[posT];
				this.titulares[posT]=this.suplentes[num];
				this.suplentes[num]=asist;
			}
		}	
	}
	
	public void Sustituir(int posT, int posS) {
		Jugadores asist=new Jugadores("vacio");
		if(this.suplentes[posS].faltas>=5) {
			System.out.println("No se puede cambiar por ese jugador, ya que tiene 5 faltas");
			if(this.titulares[posT].faltas>=5) {
				Sustituir(posT);
			}
			
		}
		else {
			asist=this.titulares[posT];
			this.titulares[posT]=this.suplentes[posS];
			this.suplentes[posS]=asist;
		}
		
	}
	
	public void verTitulares() {
		for(int pos=0;pos<this.titulares.length;pos++) {
			System.out.println("Titular " + (pos+1) + ": " + this.titulares[pos].getNombre());
			System.out.println("Faltas: " + this.titulares[pos].getFaltas());
			System.out.println("Puntos: " + this.titulares[pos].getPuntos());
		}
	}
	
	public void verSuplentes() {
		for(int pos=0;pos<this.suplentes.length;pos++) {
			System.out.println("Suplente " + (pos+1) + ": " + this.suplentes[pos].getNombre());
			System.out.println("Faltas: " + this.suplentes[pos].getFaltas());
			System.out.println("Puntos: " + this.suplentes[pos].getPuntos());
		}
	}
	
	public int CalcularPuntos() {
		int puntos=0;
		for(int pos=0;pos<this.titulares.length;pos++) {
			puntos=puntos+this.titulares[pos].getPuntos();
		}
		for(int pos=0;pos<this.suplentes.length;pos++) {
			puntos=puntos+this.suplentes[pos].getPuntos();
		}
		return puntos;
	}
	
	public int CalcularFaltas() {
		int faltas=0;
		for(int pos=0;pos<this.titulares.length;pos++) {
			faltas=faltas+this.titulares[pos].getFaltas();
		}
		for(int pos=0;pos<this.suplentes.length;pos++) {
			faltas=faltas+this.suplentes[pos].getFaltas();
		}
		return faltas;
	}
	
	public void anotar() {
		int n;
		int p;
		tec=new Scanner(System.in);
		for(int pos=0;pos<this.titulares.length;pos++) {
			System.out.println("Titular " + (pos+1) + ": " + this.titulares[pos].getNombre());
		}
		System.out.println("Cu�l jugador anot�?");
		n=tec.nextInt();
		if(n<=0 || n>this.titulares.length) {
			System.out.println("Error");
		}
		else {
			n=n-1;
			System.out.println("Cu�ntos puntos anot�? (1, 2 o 3 puntos)");
			p=tec.nextInt();
			p=(this.titulares[n].getPuntos()+p);
			this.titulares[n].setPuntos(p);
		}
	}
	
	public void anotar( int p) {
		int n;
		tec=new Scanner(System.in);
		for(int pos=0;pos<this.titulares.length;pos++) {
			System.out.println("Titular " + (pos+1) + ": " + this.titulares[pos].getNombre());
		}
		System.out.println("Qu� jugador anot�?");
		n=tec.nextInt();
		if(n<=0 || n>this.titulares.length) {
			System.out.println("Error");
		}
		else {
			n=n-1;
			p=(this.titulares[n].getPuntos()+p);
			this.titulares[n].setPuntos(p);
		}
	}
	
	public void anotar(int posT, int p) {
		p=(this.titulares[posT].getPuntos()+p);
		this.titulares[posT].setPuntos(p);
		
	}
	
	public void faltasPersonales() {
		int n;
		tec=new Scanner(System.in);
		for(int pos=0;pos<this.titulares.length;pos++) {
			System.out.println("Titular " + (pos+1) + ": " + this.titulares[pos].getNombre());
		}
		System.out.println("Cu�l jugador cometi� la falta?");
		n=tec.nextInt();
		if(n<=0 || n>this.titulares.length) {
			System.out.println("Error");
		}
		else {
			n=n-1;
			this.titulares[n].faltasPersonales();
		}
		if(this.titulares[n].faltas>=5) {
			System.out.println("El jugador deber� ser sustituido ya que tiene 5 faltas");
			Sustituir(n);
			
		}
		
	}
	
	
	
		
	
}