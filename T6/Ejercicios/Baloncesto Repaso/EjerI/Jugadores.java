package EjerI;

import java.util.Scanner;

public class Jugadores {
	private Scanner tec;
	public String nombre;
	public int puntos;
	public int faltas;
	
	public Jugadores() {
		tec=new Scanner(System.in);
		System.out.println("Como se llama");
		this.nombre=tec.next();
		this.puntos=0;
		this.faltas=0;
	}
	
	public Jugadores(String n) {
		this.nombre=n;
		this.puntos=0;
		this.faltas=0;
	}
	public Jugadores(String n, int p, int f) {
		this.nombre=n;
		this.puntos=p;
		this.faltas=f;
	}
	
	public void anotar(int p) {
		this.puntos=this.puntos+p;
	}
	public void faltasPersonales(){
		this.faltas=this.faltas+1;
		
	}
	
	public void mostrarDatos() {
		System.out.println("Nombre: " + this.nombre);
		System.out.println("Faltas: " + this.faltas);
		System.out.println("Puntos: " + this.puntos);
	}
	

	
	
	
	public String getNombre() {return nombre;}
	public void setNombre(String nombre) {this.nombre = nombre;}
	public int getPuntos() {return puntos;}
	public void setPuntos(int puntos) {this.puntos = puntos;}
	public int getFaltas() {return faltas;}
	public void setFaltas(int faltas) {this.faltas = faltas;}
	
	
	
	

	
	

}
