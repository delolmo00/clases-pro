import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Mazo {
	
	private ArrayList<Carta> cartas = new ArrayList<>();
	
	public Mazo () {
		 aņadirCartas();
	}
 public void aņadirCartas() {
	 for(int i = 1; i<10; i++) {
		 cartas.add(new Carta(i,Palo.TREBOL));
		 cartas.add(new Carta(i,Palo.CORAZON));
		 cartas.add(new Carta(i,Palo.DIAMANTE));
		 cartas.add(new Carta(i,Palo.PICA));
	 }
 }
 
 public void imprimir() {
	 System.out.println("Las cartas del mazon son");
	 System.out.println("******************************");
	 for(Carta c : cartas) {
		 System.out.println(c.getNumero()+ " "+ c.getPalo());
	 }
 }
 public void sacarUnaCartas() {
	 Random random = new Random();
     System.out.println("Una carta elegida al azar");
     
     int indice = random.nextInt(cartas.size());
    Carta carta = cartas.get(indice);
     
     carta.imprimir();
     switch (carta.retornarPalo()) {
     case CORAZON:
         System.out.println("Gana 4 puntos");
         break;
     case DIAMANTE:
         System.out.println("Gana 3 puntos");
         break;
     case PICA:
         System.out.println("Gana 2 puntos");
         break;
     case TREBOL:
         System.out.println("Gana 1 puntos");
         break;
     }

 }
}
