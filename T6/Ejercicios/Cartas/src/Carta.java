
public class Carta {
	private int numero;
    private Palo palo;

    Carta(int numero, Palo palo) {
        this.numero = numero;
        this.palo = palo;
    }
    
    

    public int getNumero() {
		return numero;
	}



	public void setNumero(int numero) {
		this.numero = numero;
	}



	public Palo getPalo() {
		return palo;
	}



	public void setPalo(Palo palo) {
		this.palo = palo;
	}



	public void imprimir() {
        System.out.println(numero + " - " + palo.name());  // palo.toString().toLowerCase());
    }

    public Palo retornarPalo() {
        return palo;
    }

}


