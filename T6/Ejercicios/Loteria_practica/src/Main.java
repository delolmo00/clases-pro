
public class Main {

	public static void main(String[] args) {
		
		Euromillones e = new Euromillones (123,"Electrónico");
		Primitiva p = new Primitiva (124,"Papel");
		
		e.iniciarSorteo();
		p.iniciarSorteo();
		try {
			p.mostrarApuesta();
		} catch (MiExcepcion e1) {
			
			System.out.println(e1.getMessage());
		}
		
		e.apostar();
		p.apostar();
		
		e.generarResultado();
		p.generarResultado();
		
		e.comprobarAciertos();
		p.comprobarAciertos();
	
	}

}
