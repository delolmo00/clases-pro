package Inmobiliarias;

public class Pisos extends Viviendas {

	protected int planta;
	protected String letra;

	public Pisos() {
		super();
		setPlanta(1); setLetra("a");}
	
    public Pisos(String calle, int numero, int metros, double precioA, double precioV, String estado, int planta, String letra) {
		super(calle, numero, metros, precioA, precioV, estado);
		setPlanta(planta); setLetra(letra);}

    public Pisos(int planta, String letra) {
    	super();
    	setPlanta(planta); setLetra(letra);}

	protected int getPlanta() {
		return planta;}

	protected void setPlanta(int planta) {
		this.planta = planta;}

	protected String getLetra() {
		return letra;}

	protected void setLetra(String letra) {
		this.letra = letra;}
	@Override
	public void venta() {
		System.out.println("Introduce el precio del metro en venta");
		double pre = t.nextDouble();
		precioV = pre * metros;
		setEstado("vendido");
		   
	}
	@Override
	public void anularVenta() {
		setEstado("disponible");
	}
	@Override
	public void mostrarPrecioAlquiler(double precioMetro) {
		setPrecioA(precioMetro * metros);
		System.out.println("El precio del alquiler es " + precioA);
		setEstado("alquilado");
	}
	@Override
	public void desalquilar() {
		setEstado("disponible");
	}
	
}
