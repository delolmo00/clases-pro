package Inmobiliarias;

import java.util.Scanner;

public class Garajes implements Alquilable, Vendible {
	private Scanner t;
	protected String calle;
	protected int numero, metros;
	protected double precioA, precioV;
	protected String estado;

	public Garajes() {
		t = new Scanner(System.in);
		setCalle("falsa");
		setNumero(123);
		setMetros(100);
		setPrecioA(1000);
		setPrecioV(100000);
		setEstado("Disponible");
	}

	public Garajes(String calle, int numero, int metros, double precioA, double precioV, String estado) {
		t = new Scanner(System.in);
		setCalle(calle);
		setNumero(numero);
		setMetros(metros);
		setPrecioA(precioA);
		setPrecioV(precioV);
		setEstado(estado);
	}

	protected String getCalle() {
		return calle;
	}

	protected void setCalle(String calle) {
		this.calle = calle;
	}

	protected int getNumero() {
		return numero;
	}

	protected void setNumero(int numero) {
		this.numero = numero;
	}

	protected int getMetros() {
		return metros;
	}

	protected void setMetros(int metros) {
		this.metros = metros;
	}

	protected double getPrecioA() {
		return precioA;
	}

	protected void setPrecioA(double precioA) {
		this.precioA = precioA;
	}

	protected double getPrecioV() {
		return precioV;
	}

	protected void setPrecioV(double precioV) {
		this.precioV = precioV;
	}

	protected String getEstado() {
		return estado;
	}

	protected void setEstado(String estado) {
		while (!estado.equalsIgnoreCase("disponible") && !estado.equalsIgnoreCase("alquilada")
				&& !estado.equalsIgnoreCase("vendida")) {
			System.out
					.println("El estado solo puede ser disponible, vendida o alquilada. Asignale uno de esos valores.");
			estado = t.next();
		}
		this.estado = estado;
	}

	@Override
	public void mostrarPrecioAlquiler(double precioMetro) {
		setPrecioA(precioMetro * metros);
		System.out.println("El precio del alquiler es " + precioA);
		setEstado("alquilado");
	}

	@Override
	public void desalquilar() {
		setEstado("disponible");
	}

	@Override
	public void vender() {
		System.out.println("Introduce el precio del metro en venta");
		double pre = t.nextDouble();
		precioV = pre * metros;
		setEstado("vendido");
	}

	@Override
	public void anularVenta() {
		setEstado("disponible");
	}

}
