package Inmobiliarias;

public interface Vendible {

	public void vender();
	public void anularVenta();
}
