package Inmobiliarias;

public interface Alquilable {

	public void mostrarPrecioAlquiler(double precio);
	
	public void desalquilar();
}
