package Inmobiliarias;

public class Adosados extends Viviendas {

	protected int metrosParcela;

	public Adosados() {
		super();
		setMetrosParcela(100);
	}

	public Adosados(int metrosParcela) {
		setMetrosParcela(metrosParcela);
	}

	public Adosados(String calle, int numero, int metros, double precioA, double precioV, String estado,
			int metrosParcela) {
		super(calle, numero, metros, precioA, precioV, estado);
		setMetrosParcela(metrosParcela);
	}

	protected int getMetrosParcela() {
		return metrosParcela;
	}

	protected void setMetrosParcela(int metrosParcela) {
		this.metrosParcela = metrosParcela;
	}

	@Override
	public void mostrarPrecioAlquiler(double precioMetro) {
		setPrecioA((precioMetro * metros) + metrosParcela);
		System.out.println("El precio del alquiler es " + precioA);
		setEstado("alquilado");
	}

	@Override
	public void vender() {
		System.out.println("Introduce el precio del metro en venta");
		double pre = t.nextDouble();
		precioV = (pre * metros) + (metrosParcela * pre);
		setEstado("vendido");
	}

	@Override
	public void venta() {
		System.out.println("Introduce el precio del metro en venta");
		double pre = t.nextDouble();
		precioV = pre * metros;
		setEstado("vendido");

	}

	@Override
	public void anularVenta() {
		setEstado("disponible");
	}
}
