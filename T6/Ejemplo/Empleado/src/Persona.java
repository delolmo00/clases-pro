
public abstract class Persona {
	private String nombre;
	private int edad;
	
	public Persona (String nombre, int edad) {
		this.nombre = nombre;
		this.edad = edad;
		
	}
	public Persona() {
		
	}
	
	public abstract void pedirVacaciones();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	public void saludar () {
		System.out.println ("Hola que tal est�s???");
	}

}
