
public class Gerente extends Empleado{
	
	private String departamento;
	
	public Gerente ( String nombre, int edad, double salario, String departamento, int vacaciones) {
		super (nombre,edad,salario,vacaciones);
		this.departamento = departamento;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	@Override
	public void pedirVacaciones() {
		
		System.out.println("Soy el gerente y he pedido mis vacaciones");
		this.setDias_vacaciones(20);
	}
	
	

}
