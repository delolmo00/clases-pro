
public class Empleado extends Persona{
	private double salario;
	private int dias_vacaciones;
	
	public Empleado (String nombre, int edad, double salario, int vacaciones ) {
		super(nombre, edad);
		this.salario = salario;
		this.dias_vacaciones = vacaciones;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public int getDias_vacaciones() {
		return dias_vacaciones;
	}

	public void setDias_vacaciones(int dias_vacaciones) {
		this.dias_vacaciones = dias_vacaciones;
	}

	@Override
	public void pedirVacaciones() {
		
		System.out.println("Soy el empleado y he pedido mis vacaciones");
		this.setDias_vacaciones(30);
		
	}
	
	

}
