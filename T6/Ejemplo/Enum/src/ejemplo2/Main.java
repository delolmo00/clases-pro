package ejemplo2;

public class Main {

	public static void main(String[] args) {
		
		Mes mes1 = Mes.AGOSTO;
		
		System.out.println(mes1 + " "+ mes1.getNumero());

		for (Mes m: Mes.values()) {
			System.out.println(m);
		}
	}

}
