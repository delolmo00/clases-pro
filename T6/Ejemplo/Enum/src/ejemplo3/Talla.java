package ejemplo3;

public enum Talla {
	
	S("Small"),M("Medium"),L("Large"),XL("Extra Large");

	private final String descripcion;
	
	Talla(String descripcion) {
		this.descripcion = descripcion;
		
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	

}
