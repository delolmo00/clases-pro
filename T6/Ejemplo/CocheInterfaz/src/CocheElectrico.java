
public class CocheElectrico extends Coche implements Electrico,Conducible{
	
	private  int capacidadBateria;
	
	
	public CocheElectrico (String modelo, String marca, String color, int capacidadBateria) {
		
		super (modelo,marca,color);
		
		this.capacidadBateria = capacidadBateria;
	}

	
	


	public int getCapacidadBateria() {
		return capacidadBateria;
	}


	public void setCapacidadBateria(int capacidadBateria) {
		this.capacidadBateria = capacidadBateria;
	}


	@Override
	public String toString() {
		return "CocheElectrico [capacidadBateria=" + capacidadBateria + "]";
	}




	@Override
	public void cargar() {
		System.out.println ("Cargando la bateria del coche el�ctrico");
		
	}



	@Override
	public void acelerar (int velocidad) {
		System.out.println("Acelerando");
	}
	
	@Override
	public void frenar() {
		System.out.println("Frenando");
	}


	@Override
	public void conectar() {
		System.out.println ("Conectando la bateria al coche el�ctrico");
		
	}
	
	
}
