
public class Main {

	public static void main(String[] args) {
		CocheElectrico tesla = new CocheElectrico("Modelo 1", "Tesla", "rojo", 100);

		tesla.conectar();
		tesla.cargar();
		tesla.acelerar(50);
		tesla.frenar();

	}

}
