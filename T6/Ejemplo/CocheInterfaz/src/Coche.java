
public class Coche {
	
	private String modelo,marca,color;
	private int  velocidad;
	
	public Coche (String modelo, String marca, String color) {
		
		this.modelo = modelo;
		this.marca = marca;
		this.color = color;
		this.velocidad = 0;
	}
	
	public void acelerar (int velocidad) {
		this.velocidad +=velocidad;
	}
	
	public void frenar() {
		this.velocidad = 0;
	}
	
	
	
	

}
