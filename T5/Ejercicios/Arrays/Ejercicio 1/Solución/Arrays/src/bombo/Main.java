package bombo;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        bombo();
    }

    private static void bombo() {

        List<String> bombo1 = new ArrayList<>();
        bombo1.add("Napoles");
        bombo1.add("Oporto");
        bombo1.add("B.Munich");
        bombo1.add("Tottenham");
        bombo1.add("Chelsea");
        bombo1.add("R.Madrid");
        bombo1.add("M.City");
        bombo1.add("Benfica");

        List<String> bombo2 = new ArrayList<>();
        bombo2.add("Brujas");
        bombo2.add("Feyenord");
        bombo2.add("Galatasaray");
        bombo2.add("Olimpiakos");
        bombo2.add("Lille");
        bombo2.add("Rayo Vallecano");
        bombo2.add("Napoles");
        bombo2.add("Sevilla");

        List<Boolean> checkPos1 = new ArrayList<>(List.of(false, false, false, false, false, false, false, false));
        List<Boolean> checkPos2 = new ArrayList<>(List.of(false, false, false, false, false, false, false, false));

        int numGenerado1, numGenerado2, contador;

        for (int i = 0; i < 8; i++) {

            do {
                numGenerado1 = (int) (Math.random() * 8);
                numGenerado2 = (int) (Math.random() * 8);

                contador = (!checkPos1.get(numGenerado1) && !checkPos2.get(numGenerado2)) ? 1 : 0;

            } while (contador == 0);

            checkPos1.set(numGenerado1, true);
            checkPos2.set(numGenerado2, true);
            System.out.println("Cruce " + (i + 1) + ": " + bombo1.get(numGenerado1) + " vs " + bombo2.get(numGenerado2));

        }
    }
}