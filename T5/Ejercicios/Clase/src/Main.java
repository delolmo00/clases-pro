import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		//Asignaturas
		Asignatura programacion = new Asignatura(1);
		Asignatura sistemas = new Asignatura(2);
		Asignatura baseDatos= new Asignatura(3);

		
		// Alumnos
		Alumno alumno1 = new Alumno(programacion, sistemas,baseDatos);
		Alumno alumno2 = new Alumno(4,5,6);
		
		Profesor profesor1 = new Profesor();
		
		
		profesor1.ponerNotas(alumno1);
		alumno1.mostrarNotas();
		System.out.println("Media del alumno "+ profesor1.calcularMedia(alumno1));
		
	}

}
