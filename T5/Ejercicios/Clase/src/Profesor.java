import java.util.Random;

public class Profesor {

	public Profesor() {

	}

	public void ponerNotas(Alumno alumno) {
		Random random = new Random();
		alumno.getAsignatura1().setCalificacion(random.nextInt(10));
		alumno.getAsignatura2().setCalificacion(random.nextInt(10));
		alumno.getAsignatura3().setCalificacion(random.nextInt(10));
	}

	public double calcularMedia(Alumno alumno) {
		double media = (alumno.getAsignatura1().getCalificacion() + alumno.getAsignatura2().getCalificacion()
				+ alumno.getAsignatura3().getCalificacion()) / 3;

		return media;
	}

}
