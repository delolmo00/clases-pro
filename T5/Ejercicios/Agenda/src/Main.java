import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Agenda agendaTrabajadores = new Agenda();
		
		Persona p1 = new Persona("Eduardo", "hhhh","44444",7777777);
		Persona p2 = new Persona("Juan", "Sanz","66669",54555);
		Persona p3 = new Persona("Mar�a", "hhhh","44544",7777877);
		Persona p4 = new Persona("Marta", "Rodriguez","66666",55555);
		agendaTrabajadores.agregarPersona(p1);
		agendaTrabajadores.agregarPersona(p2);
		agendaTrabajadores.agregarPersona(p3);
		agendaTrabajadores.agregarPersona(p4);
		agendaTrabajadores.actualizarPersona("44444",p2);
		
		Persona p5 = new Persona("Ra�l", "Milan","44444",7777777);
		Persona p6 = new Persona("Roberto", "Sanz","555",55555);
		Persona p7 = new Persona("Luisa", "hhhh","14474",7777777);
		Persona p8 = new Persona("Marta", "Rodriguez","66666",55555);
		
		Agenda agendaAmigos= new Agenda();
		agendaAmigos.agregarPersona(p5);
		agendaAmigos.agregarPersona(p6);
		agendaAmigos.agregarPersona(p7);
		agendaAmigos.agregarPersona(p8);
		
		ArrayList<Persona> agendaCoincidentes = new ArrayList<>();
		
		// A�adir a la agendaCoincidentes los contactos que tengan el mismo DNI
		
		for(Persona trabajador : agendaTrabajadores.getListaPersonas()) {
			for (Persona amigo : agendaAmigos.getListaPersonas()) {
				if (trabajador.getDni() == amigo.getDni()) {
					agendaCoincidentes.add(amigo);
				}
			}
		}
		
		
		// Mostrar las coincidentes
		 System.out.println("Las personas coincidentes son");
		 
		 for (Persona coincidente : agendaCoincidentes) {
			 System.out.println(coincidente.toString());
		 }

	}

}
