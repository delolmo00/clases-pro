import java.util.ArrayList;

public class Agenda {
	
	private ArrayList<Persona> listaPersonas;
	
	public Agenda () {
		listaPersonas = new ArrayList<>();
	}
	
	public Persona existePersona (String dni) {
		Persona personaEncontrada = null;
		/*
		for (Persona item : listaPersonas) {
			if (dni.equalsIgnoreCase(item.getDni())) {
				personaEncontrada = item;
				break;
			}
		}
		*/
		
		for (int i = 0; i<listaPersonas.size();i++) {
			if(dni.equalsIgnoreCase (listaPersonas.get(i).getDni())) {
				personaEncontrada = listaPersonas.get(i);
				break;
			}
		}
		
		return personaEncontrada;
	}
	
	public boolean agregarPersona (Persona persona) {
		boolean existe = existePersona(persona.getDni())== null;
		
		if (existe) {
			listaPersonas.add(persona);
			
		}else {
			System.out.println("No se puede a�adir a esta persona. Ya existe");
		}
		
		return existe;
	}
	
	public boolean borrarPersona(String dni) {
		Persona persona = existePersona(dni);
		if(persona!= null) {
			listaPersonas.remove(persona);
			return true;
		}else {
			System.out.println("No se puede borrar esta persona porque no existe");
		}
		return false;
	}

	public void vaciarAgenda () {
		listaPersonas.clear();
	}
	
	public void buscarPersona (String dni) {
		Persona personaEncontrada = existePersona(dni);
		
		if (personaEncontrada!= null) {
			System.out.println(personaEncontrada.toString());
		}else {
			System.out.println("No existe la persona");
		}
	}
	
	public void actualizarPersona(String dni, Persona personaNueva) {
		Persona personaEncontrada = existePersona(dni);
		if (personaEncontrada!= null) {
			personaEncontrada.setApellidos (personaNueva.getApellidos());
			personaEncontrada.setNombre (personaNueva.getNombre());
		}else {
			System.out.println("No existe la persona");
		}
	}

	public ArrayList<Persona> getListaPersonas() {
		return listaPersonas;
	}

	public void setListaPersonas(ArrayList<Persona> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}
	
	
	
}
