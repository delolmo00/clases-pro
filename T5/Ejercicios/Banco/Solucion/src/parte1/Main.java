package parte1;

public class Main {

	public static void main(String[] args) {
		Cuenta c1 = new Cuenta("Pepe", "Santander",100);
		Cuenta c2 = new Cuenta("Roberto");
		c1.ingresarDinero(200);
		c2.ingresarDinero(200);
		c1.sacarDinero(30);
		c1.mostrarSaldo();
		c2.mostrarSaldo();


	}

}
