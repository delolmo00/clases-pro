import java.util.ArrayList;

public class Equipo {

	private String nombre;
	private String estadio;
	private ArrayList<String>  trofeos ;
	
	// Constructores
	
	public Equipo (String nombre, String estadio) {
		this.nombre = nombre;
		this.estadio = estadio;
		this.trofeos = new ArrayList<>();
	}
	
	// GETTER AND SETTER
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstadio() {
		return estadio;
	}

	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}

	public ArrayList<String> getTitulos() {
		return trofeos;
	}

	public void setTitulos(ArrayList<String> titulos) {
		this.trofeos = titulos;
	}

	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", estadio=" + estadio + ", titulos=" + trofeos + "]";
	}
	
	public void aņadirTitulos(String titulo) {
		
		this.trofeos.add(titulo);
	}
}
