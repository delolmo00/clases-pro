
public class Main {

	public static void main(String[] args) {
		
		Equipo equipo1 = new Equipo ("Ateletico", "Wanda");
		Equipo equipo2 = new Equipo ("Real Madrid", "Santiago Bernabeu");

		equipo1.aņadirTitulos("Copa del rey");
		equipo1.aņadirTitulos("Uefa");
		
		equipo2.aņadirTitulos("Liga");
		equipo2.aņadirTitulos("Trofeo Santiago Bernabeu");
		
		System.out.println(equipo1.toString());
		System.out.println(equipo2.toString());
	}

}
