import java.util.ArrayList;
import java.util.Collections;

public class Persona implements Comparable{
	
	private int edad;
	private String nombre,apellidos;
	private int salario;
	private static ArrayList<Persona> personas = new ArrayList<>();
	
	
	public Persona(int edad, String nombre, String apellidos, int salario) {
		super();
		this.edad = edad;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.salario = salario;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public int getSalario() {
		return salario;
	}


	public void setSalario(int salario) {
		this.salario = salario;
	}


	public static ArrayList<Persona> getPersonas() {
		return personas;
	}


	public static void setPersonas(ArrayList<Persona> personas) {
		Persona.personas = personas;
	}

	public static void aņadirPersonas(Persona p) {
		personas.add(p);
		Collections.sort(personas);
	}

	@Override
	public int compareTo(Object o) {
		Persona otraPersona = (Persona)o;
		
		if(this.getSalario()< otraPersona.getSalario()) {
			return -1;
		}else if(this.getSalario()> otraPersona.getSalario()) {
			return 1;
		}else {
			return 0;
		}
		
	}
	
	public static void verPersonas() {
		for(Persona persona : personas) {
			System.out.println(persona.nombre + "-----"+ persona.salario);
		}
	}

}
