
public class Coche {
	
	String marca;
	String color;
	int cv;
	boolean encendido;
	double carburante = 200;
	int velocidad;
	

	// constructor 
	
	Coche (String marca, String color, int cv, boolean encendido, double carburante, int velocidad){
		this.marca = marca;
		this.color= color;
		this.cv = cv;
		this.encendido = encendido;
		this.carburante = carburante;
		this.velocidad = velocidad;
	}
	
	// Constructro vac�o
	
	Coche (){
		
	}
	
	Coche (String marca, String color){
		this.marca = marca;
		this.color = color;
		
	}
	
	 void acelerar (int speed) {
		 velocidad= velocidad + speed;
		 System.out.println("Hemos acelerado "+ speed);
		 
	 }
	 
	 void frenar (int speed) {
		 this.velocidad= velocidad - speed;
		 System.out.println("Hemos frenado "+ speed);
	 }
	 
	 void encender () {
		 if (encendido) {
			 System.out.println("El coche ya est� encendido");
		 }else {
			 encendido = true;
			 System.out.println("runn runnnn");
		 }
		 
	 }
	 void apagar () {
		 if (encendido == true) {
			 System.out.println("El coche queda apagado");
			 encendido = false;
		 }else {
			 System.out.println("El coche ya est� apagado");
		 }
	 }
	 
	 void repostar (double litros) {
		 carburante = carburante + litros;
		 System.out.println("El coche ha repostado "+ litros );
		 System.out.println("Actualmente el coche tiene "+ carburante );
	 }
	 
	 void pintar (String color) {
		 this.color = color;
		 System.out.println("El coche ha sido pintado de "+ color);
	 }
	 
}
