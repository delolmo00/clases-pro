package main;
/*
Programa una aplicación Java que permita gestionar un sistema de correo
electrónico ficticio. Para ello, debes tener en cuenta que:
• Una cuenta de correo electrónico está formada por:
▪ Una dirección de correo única
▪ Un nombre del propietario
▪ Una bandeja de entrada (colección de mensajes)
• Existe una colección de direcciones de correo pública en la que se van
introduciendo todas y cada una de las cuentas nuevas creadas con éxito.
• Una cuenta no puede crearse bajo ningún concepto si pasa algo de esto:
▪ La dirección de correo ya está cogida.
▪ El nombre del propietario está vacío o vale null.
• Una cuenta puede enviar un mensaje a otra
• Un mensaje consta de:
▪ Dirección de destino
▪ Asunto
▪ Cuerpo
• Un mensaje no puede crearse únicamente si su dirección de destino vale null,
está vacía o no tiene la forma:
algo@algo.algo
• Cuando se envía un mensaje, se comprueba que su dirección de destinatario es
válida (está en la colección pública anteriormente mencionada). Si da negativo,
dará un error en el envío. Si da positivo, entonces se añadirá a la bandeja de
entrada de la cuenta destinataria.
• Las cuentas tienen la opción de ver un listado con los asuntos y direcciones
origen de los mensajes de su bandeja de entrada.
• Si se quiere ver el contenido de un mensaje concreto, también es posible
indicando su índice en la colección.
Notas
- Usar excepciones para el tratamiento de errores de construcción o ejecución de
métodos.
- Usar encapsulación.
- Usar las colecciones adecuadas
*/

import clases.CuentaCorreo;
import misexcepciones.CuentaMalException;
import misexcepciones.MensajeMalException;

public class Main {

    public static void main(String[] args) {

        try {
            CuentaCorreo cc1 = new CuentaCorreo("cuenta1@email.com", "Alguien");
            CuentaCorreo cc2 = new CuentaCorreo("cuenta2@email.com", "Otro");

            cc1.enviar(cc2, "Bienvenido!", "Hola, este es tu primer mensaje.");
            cc2.enviar(cc1, "RE: Bienvenido", "Hola, muchas gracias.");
            cc1.enviar(cc2, "Oferta", "Cubo mediano KFC gratis. Solo hoy.");
            cc1.enviar(cc2, "Oferta", "x2 entradas de cine gratis!");

            cc1.verBandeja();
            System.out.println("");
            cc2.verBandeja();
            System.out.println("");
            cc1.verMensaje(0);
            System.out.println("");
            cc1.verMensaje(1);
            System.out.println("");
            cc2.verMensaje(1);
        } catch (CuentaMalException | MensajeMalException ex) {
            System.out.println(ex.getMessage());
        }
        
    }

}
