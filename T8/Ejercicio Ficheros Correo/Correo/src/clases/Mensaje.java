package clases;

import misexcepciones.CuentaMalException;
import misexcepciones.MensajeMalException;

public class Mensaje {
    private final String origen;
    private final String destino;
    private final String asunto;
    private final String cuerpo;

    public Mensaje(String o, String d, String a, String c) throws CuentaMalException, MensajeMalException {

        if (o == null) {
            throw new CuentaMalException("La dirección origen indicada vale 'null'.");
        } else if (o.isEmpty()) {
            throw new CuentaMalException("La dirección origen indicada está vacía.");
        } else if (d == null) {
            throw new CuentaMalException("La dirección destino indicada vale 'null'.");
        } else if (d.isEmpty()) {
            throw new CuentaMalException("La dirección destino indicada está vacía.");
        } else if (a == null) {
            throw new MensajeMalException("El asunto indicado vale 'null'.");
        } else if (a.isEmpty()) {
            throw new MensajeMalException("El asunto indicado está vacío.");
        } else if (c == null) {
            throw new MensajeMalException("El cuerpo indicado vale 'null'.");
        } else if (c.isEmpty()) {
            throw new MensajeMalException("El cuerpo indicado está vacío.");
        } else if (o.split("@").length != 2 || o.split("\\.").length != 2) {
            throw new CuentaMalException("La dirección origen no cumple el formato «algo@algo.algo».");
        } else if (d.split("@").length != 2 || d.split("\\.").length != 2) {
            throw new CuentaMalException("La dirección destino no cumple el formato «algo@algo.algo».");
        } else if (!CuentaCorreo.CUENTAS.contains(o)) {
            throw new CuentaMalException("La dirección origen no existe.");
        } else if (!CuentaCorreo.CUENTAS.contains(d)) {
            throw new CuentaMalException("La dirección destino no existe.");
        }

        origen = o;
        destino = d;
        asunto = a;
        cuerpo = c;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }

    public String getAsunto() {
        return asunto;
    }

    public String getCuerpo() {
        return cuerpo;
    }
    
    

}
