import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Archivo {
	
	/*
	 getName -- nombre escritorio
	 getPath -- la ruta
	 isDirectory
	 listFiles
	 getLenght()
	 */

	private File[] ficheros;

	public void escribir(String nombre) {

		
		try {
			String rutaActual = System.getProperty("user.dir");
			//File f = new File(rutaActual + "/" + "archivo_escritorio.txt");
			// FileWriter fr = new FileWriter ("C:\\Usuario\\Eduardo\\Java\\Dam");
			//FileWriter fr2 = new FileWriter(f);
			FileWriter fr = new FileWriter(rutaActual +  "/archivo_escritorio.txt",true);
			//FileWriter fr2 = new FileWriter("C:\\Users\\usuario\\eclipse-workspace\\Ficheros\\archivo.txt");
			BufferedWriter bw = new BufferedWriter(fr);
			bw.write(nombre);
			bw.newLine();
			
			bw.close();

			System.out.println("Escritura realizada con �xito");
		} catch (IOException e) {

			System.out.println("La ruta no es correcta");
		}

	}
	
	public void leerArchivo(String nombreArchivo) {
		
		File file = new File(nombreArchivo);
		
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bw =  new BufferedReader(fileReader);
			
			String linea;
			
			try {
				while((linea = bw.readLine())!=null) {
					System.out.println(linea);
				}
				bw.close();
			} catch (IOException e) {
				System.out.println("Error");
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("El archivo o la ruta no existen");
		}
	}
	
	public void listar(String ruta) {
		
		File f1 = new File(ruta);
		File [] ficheros = f1.listFiles();
		
		if(ficheros == null || ficheros.length == 0) {
			System.out.println("la carpeta est� vac�a");
			
		}else {
			for (int i = 0; i<ficheros.length; i++) {
				if(ficheros[i].isDirectory()) {
					System.out.println("Directorio:"+ ficheros[i].getName());
					listar(ficheros[i].getPath());
				}else {
					System.out.println(ficheros[i].getName()+"---------"+ ficheros[i].length());
				}
			}
		}
	}
	
	


}

