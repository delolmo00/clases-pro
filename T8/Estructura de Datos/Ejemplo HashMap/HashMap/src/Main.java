import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		
		Map <String,String> diccionario = new HashMap<String, String>();
		diccionario.put("rojo","red");
		diccionario.put ("verde","green");
		diccionario.put ("azul","blue");
		diccionario.put ("blanco","white");
		
		System.out.println ("Listado completo de valores");
		
		for(String valor : diccionario.values()) {
			System.out.print(valor + "-");
		}
		
		for(String clave : diccionario.keySet()) {
			System.out.print(clave + "-");
		}
		
		if(!diccionario.containsKey("negro")) {
			System.out.println("No existe la clave negro");
		}
		
		System.out.println(diccionario);
		

	}

}
