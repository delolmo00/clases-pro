
class Prenda {
    private String nombre;
    private Talla talla;

    public Prenda(String nombre, Talla talla) {
        this.nombre = nombre;
        this.talla = talla;
    }

    public String getNombre() {
        return nombre;
    }

	public Talla getTalla() {
		return talla;
	}

	public void setTalla(Talla talla) {
		this.talla = talla;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
}