
public class Persona {
	private String nombre;
	private double altura;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getAltura() {
		return altura;
	}
	public void setTalla(Double altura) {
		this.altura = altura;
	}
	public Persona(String nombre, Double altura) {
		super();
		this.nombre = nombre;
		this.altura = altura;
	}
	
	public Enum validarTalla ( ) {
		if (this.getAltura() < 1.50) {
			return Talla.S;
            
        } else if (this.getAltura() >= 1.50 && altura < 1.60) {
            return Talla.M;
        } else if (this.getAltura() >= 1.60 && altura < 1.70) {
            return Talla.L;
        } else {
            return Talla.XL;
        }
		
	}
	public boolean probarPrenda (Prenda p) {
		Enum tallaAdecuada = validarTalla();
		if (p.getTalla()== tallaAdecuada) {
			System.out.println ("Te queda bien esta talla");
			return true;
		}else {
			System.out.println ("No es tu talla");
			return false;
		}
	}

}
