package b;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Aplicacion app = new Aplicacion();
        System.out.print("Ingrese el nombre del archivo a abrir: ");
        String archivo = scanner.nextLine();

        File archivoFile = new File(archivo);

        if (archivoFile.exists()) {
            System.out.println("Operaciones disponibles:");
            System.out.println("1. Leer contenido del archivo.");
            System.out.println("2. A�adir contenido al final del archivo.");
            System.out.println("3. Reescribir el contenido del archivo.");
            System.out.println("4. Mostrar propiedades del archivo.");
            System.out.println("5. Salir.");
            System.out.print("Ingrese el n�mero de la operaci�n deseada: ");
            int opcion = scanner.nextInt();
            scanner.nextLine(); // Limpiar el buffer de entrada

            switch (opcion) {
                case 1:
                    app.leerArchivo(archivo);
                    break;
                case 2:
                    app.a�adirAlArchivo(archivo);
                    break;
                case 3:
                    app.escribirArchivo(archivo);
                    break;
                case 4:
                    app.propiedadesArchivo(archivoFile);
                    break;
                case 5:
                    System.out.println("Saliendo...");
                    break;
                default:
                    System.out.println("Opci�n inv�lida. Saliendo...");
                    break;
            }
        } else {
            System.out.println("El archivo no existe. Saliendo...");
        }
    }
}
