package b;

import java.io.*;
import java.util.Scanner;



public class Aplicacion {
    public void leerArchivo(String archivo) {
        try {
        	FileReader fr = new FileReader(archivo);
            BufferedReader breader = new BufferedReader(fr);
            String linea;
            System.out.println("Contenido del archivo " + archivo + ":");
            while ((linea = breader.readLine()) != null) {
                System.out.println(linea);
            }
            breader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error: Archivo no encontrado.");
        } catch (IOException e) {
            System.out.println("Error de entrada/salida: " );
        }
    }

    public void a�adirAlArchivo(String archivo) {
        try {
        	FileWriter fw = new FileWriter(archivo,true); // true permite a�adir al archivo, no sobrescribe
            BufferedWriter bwriter = new BufferedWriter(fw);
            Scanner scanner = new Scanner(System.in);
            String contenido;
            System.out.println("Ingrese el contenido a a�adir al archivo (m�ximo 10 l�neas o 'salir' para finalizar):");
            while (scanner.hasNextLine()) {
                contenido = scanner.nextLine();
                if (contenido.equalsIgnoreCase("salir") || bwriter.append(contenido).append("\n").toString().getBytes().length > 10) {
                    break;
                }
            }
            bwriter.close();
            System.out.println("Contenido a�adido al archivo exitosamente.");
        } catch (FileNotFoundException e) {
            System.out.println("Error: Archivo no encontrado.");
        } catch (IOException e) {
            System.out.println("Error de entrada/salida: " + e.getMessage());
        }
    }

    public void escribirArchivo(String archivo) {
        try {
        	FileWriter fw = new FileWriter(archivo);
            BufferedWriter bwriter = new BufferedWriter(fw);
            Scanner scanner = new Scanner(System.in);
            String contenido;
            System.out.println("Ingrese el contenido para escribir en el archivo (m�ximo 10 l�neas o 'salir' para finalizar):");
            while (scanner.hasNextLine()) {
                contenido = scanner.nextLine();
                if (contenido.equalsIgnoreCase("salir") || bwriter.append(contenido).append("\n").toString().getBytes().length > 10) {
                    break;
                }
            }
            bwriter.close();
            System.out.println("Contenido escrito en el archivo exitosamente.");
        } catch (IOException e) {
            System.out.println("Error de entrada/salida: " + e.getMessage());
        }
    }

    public void propiedadesArchivo(File archivo) {
        if (archivo.exists()) {
            System.out.println("Propiedades del archivo:");
            System.out.println("Nombre del archivo: " + archivo.getName());
            System.out.println("Ruta absoluta: " + archivo.getAbsolutePath());
            System.out.println("Tama�o: " + archivo.length() + " bytes");
            
        } else {
            System.out.println("El archivo no existe.");
        }
    }
}