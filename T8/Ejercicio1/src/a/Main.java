package a;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String nombre, apellido, edad;

		while (true) {
			System.out.print("Ingresa el nombre (o 'salir' para detener la ejecuci�n): ");
			nombre = scanner.nextLine();
			if (nombre.equalsIgnoreCase("salir")) {
				break;
			}

			System.out.print("Ingresa el apellido: ");
			apellido = scanner.nextLine();

			System.out.print("Ingresa la edad: ");
			edad = scanner.nextLine();

			// Escribir los datos en el archivo usuarios.txt
			try {
				FileWriter fw = new FileWriter("usuarios.txt", true);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("Nombre: " + nombre + "\n");
				bw.write("Apellido: " + apellido + "\n");
				bw.write("Edad: " + edad + "\n");
				bw.write("--------------------\n");
				bw.close();
			} catch (IOException e) {
				System.out.println("Error al escribir en el archivo usuarios.txt: ");
			}
		}

		System.out.println("�Hasta luego!");
	}
}

