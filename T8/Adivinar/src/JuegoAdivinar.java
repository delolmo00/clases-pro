
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class JuegoAdivinar {
	public static ArrayList<Persona> jugadores = new ArrayList<>();

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		boolean seguirJugando = true;

		while (seguirJugando) {
			System.out.println("�Bienvenido al juego de adivinanza!");
			System.out.print("Por favor, introduce tu nombre: ");
			String nombreJugador = scanner.nextLine();

			int numeroAleatorio = generarNumeroAleatorio();
			int intentos = 0;
			boolean adivinado = false;

			while (!adivinado) {
				System.out.print("Introduce tu respuesta (un n�mero entre 0 y 20): ");
				int respuesta = scanner.nextInt();
				intentos++;

				if (respuesta == numeroAleatorio) {
					adivinado = true;
					System.out.println("�Felicidades, has adivinado el n�mero!");
					System.out.println("Nombre: " + nombreJugador);
					System.out.println("Intentos: " + intentos);

					// Escribir en el fichero juego.txt
					escribirEnFichero(nombreJugador, intentos,"resultado.txt");

					scanner.nextLine(); // Limpiar el buffer de entrada
				} else if (respuesta < numeroAleatorio) {
					System.out.println("El n�mero es mayor.");
				} else {
					System.out.println("El n�mero es menor.");
				}
			}

			System.out.print("�Quieres seguir jugando? (si/no): ");
			String seguir = scanner.nextLine();

			if (!seguir.equalsIgnoreCase("si")) {
				seguirJugando = false;
			}
		}

		scanner.close();
		System.out.println("�Gracias por jugar!");
	}

	// Generar un n�mero aleatorio entre 0 y 20
	public static int generarNumeroAleatorio() {
		return (int) (Math.random() * 21);
	}

	// Escribir en el fichero juego.txt
	public static void escribirEnFichero(String nombre, int intentos,String fichero) {
		try {
			FileWriter fw = new FileWriter(fichero, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(nombre + "," + intentos);
			bw.close();

		} catch (IOException e) {
			System.out.println("Error al escribir en el fichero: " + e.getMessage());
		}
	}

	public static void leerGanador(String fichero) throws MiExcepcion {
		if ((fichero.charAt(0) == 'a') || fichero.endsWith("o")) {
			throw new MiExcepcion("No puede empezar por a o terminar por o");

		} else {
			try {
				FileReader fr = new FileReader(fichero);
				BufferedReader br = new BufferedReader(fr);
				String linea;
				while ((linea = br.readLine()) != null) {
					String[] persona = linea.split(", ");
					Persona p = new Persona(persona[0], Integer.parseInt(persona[1]));
					jugadores.add(p);
					

				}
				Collections.sort(jugadores);
				System.out
						.println("El jugador que m�s r�pido ha adivinado el n�mero es " + jugadores.get(0).getNombre());
			} catch (IOException e) {
				System.out.println("Error al escribir en el fichero: " + e.getMessage());
				
			}
		}
	}
}
