import java.util.ArrayList;

public class Persona implements Comparable{
	private String nombre;
	private int intentos;
	
	
	
	
	public Persona(String nombre, int intentos) {
		
		this.nombre = nombre;
		this.intentos = intentos;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIntentos() {
		return intentos;
	}
	public void setIntentos(int aciertos) {
		this.intentos = aciertos;
	}
	@Override
    public int compareTo(Object o) {
        
        Persona otraPersona = (Persona)o;
        
        if(this.getIntentos()<otraPersona.getIntentos()){
            return -1;
        }else if (this.getIntentos()>otraPersona.getIntentos()){
            return 1;
        }else{
            return 0;
        }
    }
	

}
