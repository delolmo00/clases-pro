import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

	public static void main(String[] args) {
		ArrayList<String> animales = new ArrayList<>();
		String nombre= "lectura.txt";
		ordenarFichero(animales,nombre);
		
		for(String animal : animales) {
			System.out.println(animal);
		}

		
	}
	
	public static void ordenarFichero(ArrayList<String> animales, String nombreArchivo) {
		
		File file = new File (nombreArchivo);
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader (fr);
			
			String linea;
			try {
				while((linea = br.readLine())!=null) {
					animales.add(linea);
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Collections.sort(animales);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
